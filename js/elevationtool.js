define([
        "dojo/Evented",
        "dojo/_base/declare",
        "dojo/_base/lang",
        "dojo/has", // feature detection
        "esri/kernel", // esri namespace
        "dijit/_WidgetBase",
        "dijit/a11yclick", // Custom press, release, and click synthetic events which trigger on a left mouse click, touch, or space/enter keyup.
        "dijit/_TemplatedMixin",
        "dojo/on",
        "dojo/Deferred",
        "dojo/text!elevationtool-gp-js/dijit/templates/elevationtool.html", // template html
        "dojo/text!elevationtool-gp-js/dijit/templates/dataextract.html", // template html
        "dojo/text!elevationtool-gp-js/dijit/templates/datadownload.html", // template html
        "dojo/i18n!elevationtool-gp-js/nls/jsapi", // localization
        "dijit/registry",
        "dojo/dom",
        "dojo/dom-class",
        "dojo/dom-style",
        "esri/toolbars/draw",
        "esri/graphic",
        "esri/symbols/SimpleFillSymbol",
        "esri/tasks/FeatureSet",
        "esri/SpatialReference",
        "esri/geometry/Polygon",
        "esri/geometry/Point",
        "esri/geometry/Multipoint",
        "dijit/Dialog",
        "dojox/widget/Standby",
        "dojox/dtl/_base",
        "dojox/dtl/tag/logic",
        "dojox/dtl/Context"
    ],
    function(
        Evented,
        declare,
        lang,
        has, esriNS,
        _WidgetBase, a11yclick, _TemplatedMixin,
        on,
        Deferred,
        dijitTemplate, dataextractTemplate, datadownloadTemplate, i18n,
        registry, dom, domClass, domStyle,
        Draw, Graphic, SimpleFillSymbol, FeatureSet, SpatialReference,
        Polygon, Point, Multipoint, Dialog, Standby, dtl, logic, Context
    ) {
        var Widget = declare("esri.dijit.elevationtool", [_WidgetBase, _TemplatedMixin, Evented], {

            // template HTML
            templateString: dijitTemplate,

            // default options
            options: {
                theme: "elevationtool",
                map: null,
                tb: null, // toolbar
                aoi: null, // AOI
                deOpts: null, // Data Extract parameters.
                gpAOIinfo: null, // Synchronous GP service for AOI info.
                gpAOIpolygon: null, // Synchronous GP service for AOI polygon.
                gpDataExtract: null, // Asynchronous GP service for Data Extract.
                resultsAOIinfo: null,
                resultsAOIpolygon: null,
                resultsDataExtract: null,
                visible: true
            },

            // lifecycle: 1
            constructor: function(options, srcRefNode) {
                // mix in settings and defaults
                var defaults = lang.mixin({}, this.options, options);

                // widget node
                this.domNode = srcRefNode;

                // store localized strings
                this._i18n = i18n;

                // properties
                this.set("theme", defaults.theme);
                this.set("map", defaults.map);
                this.set("tb", defaults.tb);
                this.set("aoi", defaults.aoi);
                this.set("deOpts", defaults.deOpts);
                this.set("gpAOIinfo", defaults.gpAOIinfo);
                this.set("resultsAOIinfo", defaults.resultsAOIinfo);
                this.set("gpAOIpolygon", defaults.gpAOIpolygon);
                this.set("resultsAOIpolygon", defaults.resultsAOIpolygon);
                this.set("gpDataExtract", defaults.gpDataExtract);
                this.set("resultsDataExtract", defaults.resultsDataExtract);
                this.set("visible", defaults.visible);

                // listeners
                this.watch("theme", this._updateThemeWatch);
                this.watch("aoi", this._updateAOIwatch);
                this.watch("deOpts", this._updateDeOptswatch);
                this.watch("resultsAOIinfo", this._resultsAOIwatch);
                this.watch("resultsAOIpolygon", this._resultsAOIpolygonwatch);
                this.watch("resultsDataExtract", this._resultsDeOptswatch);
                this.watch("visible", this._visible);

                // classes
                this._css = {
                    container: "elevationContainer",
                    sketch: "sketch",
                    huc: "huc",
                    clu: "clu",
                    loading: "loading"
                };
            },
            // bind listener for button to action
            postCreate: function() {
                this.inherited(arguments);
                this.own(
                    on(this._sketchNode, a11yclick, lang.hitch(this, this.sketch)),
                    on(this._hucNode, a11yclick, lang.hitch(this, this.point)),
                    on(this._cluNode, a11yclick, lang.hitch(this, this.multipoint))
                );
            },
            // start widget. called by user
            startup: function() {
                // map not defined
                if (!this.map) {
                    this.destroy();
                    console.log("elevationtool::map required");
                }
                // when map is loaded
                if (this.map.loaded) {
                    this._init();
                } else {
                    on.once(this.map, "load", lang.hitch(this, function() {
                        this._init();
                    }));
                }

                // gpAOIinfo not defined
                if (!this.gpAOIinfo) {
                    this.destroy();
                    console.log("elevationtool::gpAOIinfo required");
                }
                // TODO: get rid of passing widgetRef and replace with a cleaner approach.
                this.gpAOIinfo.widgetRef = this; // allows us to have a reference
                this.gpAOIinfo.on("error", function(evt) {
                    console.log("elevationtool::" + evt.error.message);
                });
                this.gpAOIinfo.on("execute-complete", function(evt) {
                    evt.target.widgetRef._set("resultsAOIinfo", {
                        "spatial_refs": evt.results[0].value,
                        "datasets": evt.results[1].value,
                        "domID": evt.target.widgetRef.domNode.id
                    });
                });

                // gpAOIpolygon not defined
                if (!this.gpAOIpolygon) {
                    this.destroy();
                    console.log("elevationtool::gpAOIpolygon required");
                }
                // TODO: get rid of passing widgetRef and replace with a cleaner approach.
                this.gpAOIpolygon.widgetRef = this; // allows us to have a reference
                this.gpAOIpolygon.on("error", function(evt) {
                    console.log("elevationtool::" + evt.error.message);
                });
                this.gpAOIpolygon.on("execute-complete", function(evt) {

                    var response = evt.results[0].value;
                    var symbol = new SimpleFillSymbol();
                    var featureset = new FeatureSet();

                    featureset.fields = [];
                    featureset.geometryType = "esriGeometryPolygon";
                    featureset.features = [];

                    for (var i = 0; i < response.length; i++) {
                        var polygon = new Polygon(response[i]);
                        polygon.setSpatialReference(new SpatialReference(3857));
                        var graphic = new Graphic(polygon, symbol);
                        featureset.features.push(graphic);
                        evt.target.widgetRef.map.graphics.add(graphic);
                    }

                    evt.target.widgetRef._set("aoi", {
                        "Features": featureset
                    });
                });

                // gpDataExtract not defined
                if (!this.gpDataExtract) {
                    this.destroy();
                    console.log("elevationtool::gpDataExtract required");
                }

                // TODO: get rid of passing widgetRef and replace with a cleaner approach.
                this.gpDataExtract.widgetRef = this; // allows us to have a reference
                this.gpDataExtract.on("status-update", function(evt) {
                    console.log("elevationtool::" + evt.jobInfo.jobStatus);
                    var standby = registry.byId(evt.target.widgetRef.domNode.id + "_standby");

                    if (evt.jobInfo.jobStatus === "esriJobSubmitted") {
                        standby.startup();
                        standby.show();
                    }

                    if (evt.jobInfo.jobStatus === "esriJobFailed") {
                        standby.hide();
                    }

                    if (evt.jobInfo.jobStatus === "esriJobSucceeded") {
                        standby.hide();
                    }

                });
                this.gpDataExtract.on("job-complete", function(evt) {
                    evt.target.getResultData(evt.jobInfo.jobId, "Zipfile",
                        function(result) {
                            evt.target.widgetRef._set("resultsDataExtract", result);
                        });
                });
                this.dialog = new Dialog({
                    title: null,
                    content: null,
                    id: this.domNode.id + "_dialog"
                });

                // Standby grays-out the screen while processing
                var standby = new Standby({
                    id: this.domNode.id + "_standby",
                    target: dojo.body(),
                    zIndex: 999
                });
                document.body.appendChild(standby.domNode);
            },
            // connections/subscriptions will be cleaned up during the destroy() lifecycle phase
            destroy: function() {
                this.inherited(arguments);
            },
            /* ---------------- */
            /* Public Events */
            /* ---------------- */
            // sketch
            // load
            /* ---------------- */
            /* Public Functions */
            /* ---------------- */
            sketch: function() {
                // deferred to return
                var def = new Deferred();

                // Add code here
                this.tb.activate(Draw.POLYGON);

                if (this.map.graphics.graphics.length) {
                    this.map.graphics.clear();
                }

                return def.promise;
            },
            point: function() {
                // deferred to return
                var def = new Deferred();

                // Add code here
                this.tb.activate(Draw.POINT);

                if (this.map.graphics.graphics.length) {
                    this.map.graphics.clear();
                }

                return def.promise;
            },
            multipoint: function() {
                // deferred to return
                var def = new Deferred();

                // Add code here
                this.tb.activate(Draw.MULTI_POINT);

                if (this.map.graphics.graphics.length) {
                    this.map.graphics.clear();
                }

                return def.promise;
            },
            // show widget
            show: function() {
                this.set("visible", true);
            },
            // hide widget
            hide: function() {
                this.set("visible", false);
            },
            /* ---------------- */
            /* Private Functions */
            /* ---------------- */
            _init: function() {
                // show or hide widget
                this._visible();

                // widget is now loaded
                this.set("loaded", true);
                this.emit("load", {});

                this.tb = new Draw(this.map, {
                    "tooltipOffset": 25
                });

                // TODO: get rid of passing widgetRef and replace with a cleaner approach.
                this.tb.widgetRef = this; // allows us to have a reference
                this.tb.on("draw-end", this._addToMap);
            },
            // theme changed
            _updateThemeWatch: function(attr, oldVal, newVal) {
                domClass.remove(this.domNode, oldVal);
                domClass.add(this.domNode, newVal);
            },
            // show or hide widget
            _visible: function() {
                if (this.get("visible")) {
                    domStyle.set(this.domNode, "display", "block");
                } else {
                    domStyle.set(this.domNode, "display", "none");
                }
            },
            // adds user sketch to the map
            _addToMap: function(evt) {
                evt.target.deactivate();

                // user sketch
                if (evt.geometry.type === "polygon") {

                    var symbol = new SimpleFillSymbol();
                    var graphic = new Graphic(evt.geometry, symbol);

                    evt.target.map.graphics.add(graphic);

                    var featureset = new FeatureSet();
                    featureset.fields = [];
                    featureset.geometryType = "esriGeometryPolygon";
                    featureset.features = [graphic];

                    evt.target.widgetRef._set("aoi", {
                        "Features": featureset
                    });
                }

                // point
                if (evt.geometry.type === "point") {

                    var point = new Point(evt.geometry);
                    var graphic = new Graphic(point);

                    var featureset = new FeatureSet();
                    featureset.fields = [];
                    featureset.geometryType = "esriGeometryPoint";
                    featureset.features = [graphic];

                    evt.target.widgetRef.gpAOIpolygon.execute({
                        "Features": featureset,
                        "Dataset": "HUC12"
                    });
                }

                // multipoint
                if (evt.geometry.type === "multipoint") {

                    var multipoint = new Multipoint(evt.geometry);

                    var featureset = new FeatureSet();
                    featureset.fields = [];
                    featureset.geometryType = "esriGeometryPoint";
                    featureset.features = [];

                    for (var i = 0; i < multipoint.points.length; i++) {
                        var point = new Point(multipoint.points[i]);
                        point.setSpatialReference(new SpatialReference(3857));
                        var graphic = new Graphic(point);
                        featureset.features.push(graphic);
                    }

                    evt.target.widgetRef.gpAOIpolygon.execute({
                        "Features": featureset,
                        "Dataset": "CLU"
                    });
                }

            },
            // called when "aoi" changes
            _updateAOIwatch: function(attr, oldVal, newVal) {
                this.gpAOIinfo.execute(newVal);
            },
            // called when "deOpts" (Data extract options) changes
            _updateDeOptswatch: function(attr, oldVal, newVal) {
                this.gpDataExtract.submitJob(newVal);
            },
            // called when "resultsAOIinfo" changes
            _resultsAOIwatch: function(attr, oldVal, newVal) {
                var template = new dtl.Template(dataextractTemplate);
                var context = new Context(newVal);
                this.dialog.set("title", "Submit a data request");
                this.dialog.set("content", template.render(context));
                this.dialog.show();
            },
            // called when "resultsAOIpolygon" changes
            _resultsAOIpolygonwatch: function(attr, oldVal, newVal) {
                return;
            },
            // called when "resultsDataExtract" changes
            _resultsDeOptswatch: function(attr, oldVal, newVal) {
                var template = new dtl.Template(datadownloadTemplate);
                var context = new Context(newVal);
                this.dialog.set("title", "Download data");
                this.dialog.set("content", template.render(context));
                this.dialog.show();
            }
        });
        if (has("extend-esri")) {
            lang.setObject("dijit.elevationtool", Widget, esriNS);
        }
        return Widget;
    });
