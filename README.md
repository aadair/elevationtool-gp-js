# elevationtool-gp-js

elevationtool-gp-js is an attempt at creating a Dojo dijit that can be dropped into our existing GeoObserver Elevation JavaScript application.

Currently, GeoObserver Elevation does not have a way of interfacing the geoprocessing services hosted on gisdev2.

## Instructions

You need Python and flask installed to run the development server.

Example usage:

    python server.py

